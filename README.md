# pyMIBCAN

A python 3 interface to MIB3 CAN Protocol.

# Description

MIB is some sort of abbreviation for Medien-Informations-Bus or media information bus.

It became the de-facto standard in most German cars since MOST bus has died.

Almost every electronic device that is not part of integral car systems but multimedia related is connected to this bus.

This project is starting out as a demonstration that an application for MIB3 can be written with minimal effort with python.

It may evolve along the way.
