"""
A user space adapter to write MIB3 touch events into
a UInput device.
(c) Patrick Menschel 2021
"""
from typing import Optional
from subprocess import CalledProcessError, check_output


def is_multitouch_module_available() -> Optional[bool]:
    """
    check if hid-multitouch module is loaded
    :return: True if found by lsmod
    """
    try:
        return len(check_output("lsmod | grep hid-multitouch", shell=True).decode()) > 0
    except CalledProcessError:
        pass
    return


if __name__ == "__main__":
    import argparse

    from evdev import UInput, AbsInfo, ecodes as e

    from queue import Empty

    from simple_client import SimpleMibClient

    HID_DU_4_ZR_REQ = 0x17F8F473
    HID_DU_4_ZR_RESP = 0x17FA73F4
    # A Standard TP_Channel

    parser = argparse.ArgumentParser()
    parser.add_argument("interface")
    args = parser.parse_args()
    mibclient = SimpleMibClient(can_interface=args.interface,
                                rx_addr=HID_DU_4_ZR_REQ,
                                tx_addr=HID_DU_4_ZR_RESP)

    # capabilities taken from a pollin display set which uses egalax touch controller
    # very disappointing, it does emulate a mouse instead of a multi touch device
    # print(egalax)
    # device /dev/input/by-id/usb-eGalax_Inc._Touch-event-mouse, name "eGalax Inc. Touch", phys "usb-0000:1e:00.3-2/input0"
    # egalax.capabilities(verbose=True)
    # {('EV_SYN', 0): [('SYN_REPORT', 0), ('SYN_CONFIG', 1), ('SYN_DROPPED', 3), ('?', 4)],
    #  ('EV_KEY', 1): [(['BTN_LEFT', 'BTN_MOUSE'], 272), ('BTN_RIGHT', 273)],
    #  ('EV_ABS', 3): [(('ABS_X', 0), AbsInfo(value=0, min=42, max=1981, fuzz=0, flat=0, resolution=0)),
    #                  (('ABS_Y', 1), AbsInfo(value=0, min=42, max=1981, fuzz=0, flat=0, resolution=0))],
    #  ('EV_MSC', 4): [('MSC_SCAN', 4)]}

    display_resolution = (1024, 768)
    # Note: MIB3 Touch protocol is so badly designed that the client must know the display size of the touch display.
    #       If a display is bigger than 10bit 1024x1024, the raw value should be multiplied by 2 and so on.

    mode = "mouse"
    if is_multitouch_module_available:
        mode = "multitouch"

    if mode == "multitouch":
        cap = {
            e.EV_ABS: [
                (e.ABS_MT_SLOT, AbsInfo(value=0, min=0, max=10, fuzz=0, flat=0, resolution=0)),  # 1 slot for each finger
                (e.ABS_MT_POSITION_X, AbsInfo(value=0, min=0, max=display_resolution[0], fuzz=0, flat=0, resolution=0)),
                (e.ABS_MT_POSITION_Y, AbsInfo(value=0, min=0, max=display_resolution[1], fuzz=0, flat=0, resolution=0)),
                (e.ABS_MT_PRESSURE, AbsInfo(value=0, min=0, max=255, fuzz=0, flat=0, resolution=0)),
            ]
        }
        with UInput(cap, name="MIB_MT_Device", version=1) as ui:
            try:
                while True:
                    try:
                        touch_data = mibclient.get_next()
                    except Empty:
                        print("Nothing")
                    else:
                        for finger_id, finger_data in touch_data.get("fingers").items():
                            x_pos, y_pos, z_pos = finger_data

                            ui.write(e.EV_ABS, e.ABS_MT_SLOT, finger_id)
                            ui.write(e.EV_ABS, e.ABS_MT_TRACKING_ID, finger_id)
                            ui.write(e.EV_ABS, e.ABS_MT_POSITION_X, x_pos)
                            ui.write(e.EV_ABS, e.ABS_MT_POSITION_Y, y_pos)
                            # ui.write(e.EV_ABS, e.ABS_MT_PRESSURE, z_pos) # don't use pressure for now
                        ui.syn()

            except KeyboardInterrupt:
                pass

    elif mode == "mouse":
        cap = {
            e.EV_ABS: [
                (e.ABS_X, AbsInfo(value=0, min=0, max=display_resolution[0], fuzz=0, flat=0, resolution=0)),
                (e.ABS_Y, AbsInfo(value=0, min=0, max=display_resolution[1], fuzz=0, flat=0, resolution=0)),
            ]
        }
        with UInput(cap, name="MIB_Mouse_Device", version=1) as ui:
            try:
                while True:
                    try:
                        touch_data = mibclient.get_next()
                    except Empty:
                        print("Nothing")
                    else:
                        finger_data = touch_data.get("fingers").get(0)
                        x_pos, y_pos, z_pos = finger_data
                        ui.write(e.EV_ABS, e.ABS_X, x_pos)
                        ui.write(e.EV_ABS, e.ABS_Y, y_pos)
                        ui.syn()

            except KeyboardInterrupt:
                pass

# example from https://python-evdev.readthedocs.io/en/latest/tutorial.html#injecting-input
# from evdev import UInput, ecodes as e
# ui = UInput()
# ui.write(e.EV_KEY, e.KEY_A, 1)  # KEY_A down
# ui.syn()
# ui.close()

# example from https://www.kernel.org/doc/html/latest/input/multi-touch-protocol.html
# ABS_MT_SLOT 0
# ABS_MT_TRACKING_ID 45
# ABS_MT_POSITION_X x[0]
# ABS_MT_POSITION_Y y[0]
# ABS_MT_SLOT 1
# ABS_MT_TRACKING_ID 46
# ABS_MT_POSITION_X x[1]
# ABS_MT_POSITION_Y y[1]
# SYN_REPORT
