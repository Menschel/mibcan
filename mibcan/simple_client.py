"""
Simple client for MIB3 Protocol
(c) Patrick Menschel 2021
"""

from queue import Queue
from socketcan import CanIsoTpSocket
import threading

from mib3_common import data_to_finger_pos


class SimpleMibClient:

    def __init__(self,
                 can_interface: str,
                 tx_addr: int,
                 rx_addr: int):
        """
        Constructor
        :param can_interface: The name of the can interface like "can0".
        :param tx_addr: The transmit address of the isotp channel.
        :param rx_addr: The receive address of the isotp channel.
        """
        self.can_interface = can_interface
        self.rx_socket = CanIsoTpSocket(interface=self.can_interface,
                                        tx_addr=tx_addr,
                                        rx_addr=rx_addr,
                                        use_padding=True)
        self.rx_queue = Queue()
        self.rx_handler = threading.Thread(target=self.handle_rx)
        self.rx_handler.setDaemon(True)
        self.rx_handler.start()

    def handle_rx(self) -> None:
        """ rx thread """
        while True:
            data = self.rx_socket.recv(5000)
            self.rx_queue.put(data_to_finger_pos(data))

    def get_next(self, timeout=1):
        return self.rx_queue.get(timeout=timeout)


if __name__ == "__main__":
    import argparse

    from queue import Empty

    HID_DU_4_ZR_REQ = 0x17F8F473
    HID_DU_4_ZR_RESP = 0x17FA73F4
    # A Standard TP_Channel

    parser = argparse.ArgumentParser()
    parser.add_argument("interface")
    args = parser.parse_args()
    mibclient = SimpleMibClient(can_interface=args.interface,
                                rx_addr=HID_DU_4_ZR_REQ,
                                tx_addr=HID_DU_4_ZR_RESP)
    try:
        while True:
            try:
                print(mibclient.get_next())
            except Empty:
                print("Nothing")

    except KeyboardInterrupt:
        pass