"""
Common functions for MIB3 Protocol
(c) Patrick Menschel 2021
"""


def data_to_finger_pos(data: bytes):
    """
    Convert a MIB3 touch data message into
    touch informations
    :param data: The raw bytes from the CAN message.
    :return: A dictionary with keys "timestamp", "fingers".
             "fingers" is a dictionary containing a (x,y,z) tuple
             for each "finger_id"
    """
    cmd = data[0]
    assert cmd == 0xA0
    timestamp = data[1]
    updatefingercount = data[2] >> 4
    fingercount = data[2] & 0xF
    finger_dict = {}
    for idx in range(3, len(data), 4):
        finger_id = data[idx] >> 4
        xpos = (data[idx] << 4) + (data[idx + 1] >> 2)
        ypos = ((data[idx + 1] & 0x3) << 8) + data[idx + 2]
        zpos = data[idx + 3]
        finger_dict.update({finger_id: (xpos, ypos, zpos)})
    assert updatefingercount == len(finger_dict)
    return {"timestamp": timestamp,
            "fingers": finger_dict}


